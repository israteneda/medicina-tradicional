﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MedicinaTradicional.Startup))]
namespace MedicinaTradicional
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

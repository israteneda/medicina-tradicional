﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using MedicinaTradicional.Entities;

namespace MedicinaTradicional.Database
{
    public class CBContext : DbContext
    {
        public CBContext() : base("MedicinaTradicionalConnection")
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
    